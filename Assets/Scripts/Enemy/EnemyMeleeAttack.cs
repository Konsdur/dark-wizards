using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyMeleeAttack : MonoBehaviour
{
    public EnemyStats enemyStats;
    public PlayerHealthBar playerHealthBar;
    public PlayerStats playerStats;
    Transform target;
    NavMeshAgent agent;
    public bool inReach;
    public bool damageTaken;
    public float damageValue;
    void Awake()
    {
        GameObject player = GameObject.FindWithTag("Player");
        GameObject healthBar = GameObject.FindWithTag("HealthBar");
        playerStats = player.GetComponent<PlayerStats>();
        playerHealthBar = healthBar.GetComponent<PlayerHealthBar>();
    }
    void Start()
    {
        target = PlayerManager.instance.player.transform;
        agent = GetComponent<NavMeshAgent>();
    }
    void Update()
    {
        float distance = Vector3.Distance(target.position, transform.position);
        if(distance <= agent.stoppingDistance)
        {
            inReach = true;
        }
        if(distance > agent.stoppingDistance)
        {
            inReach = false;
        }
        if(damageTaken == false && inReach == true)
        {
            TakeDamage();
            StartCoroutine("DamagePerTick");
        }
    }
    IEnumerator DamagePerTick()
    {
        damageTaken = true;
        yield return new WaitForSeconds(3f);
        damageTaken = false;
    }
    void TakeDamage()
    {
        damageValue = ((enemyStats.BasicDamage*enemyStats.BasicDamageMultiplier)-playerStats.Armor);
        if(playerStats.Health>0 && damageValue > 0)
        {
            playerStats.Health -= damageValue;
            playerHealthBar.UpdateValues();
        }
    }   
}

