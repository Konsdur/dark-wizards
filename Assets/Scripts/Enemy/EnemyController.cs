using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour
{
    Transform target;
    NavMeshAgent agent;
    EnemyStats stats;
    void Start()
    {
        target = PlayerManager.instance.player.transform;
        agent = GetComponent<NavMeshAgent>();
        stats = GetComponent<EnemyStats>();
    }
    void Update()
    {
        agent.speed = stats.MovementSpeed;
        float distance = Vector3.Distance(target.position, transform.position);
        agent.SetDestination(target.position);

            if(distance <= agent.stoppingDistance)
            {
                //Attack the target
                FaceTarget();
            }
    }
    void FaceTarget()
    {
        Vector3 direction = (target.position-transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);
    }
}
