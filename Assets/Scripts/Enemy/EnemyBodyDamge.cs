using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBodyDamge : MonoBehaviour
{
    public EnemyStats enemyStats;
    public PlayerStats playerStats;
    private static PlayerHealthBar playerHealthBar;
    public bool damageTaken = false;
    public bool onTrigger = false;
    void Awake()
    {
        GameObject player = GameObject.FindWithTag("Player");
        playerStats = player.GetComponent<PlayerStats>();
    }
    void Start()
    {
        playerHealthBar = GameObject.FindObjectOfType(typeof(PlayerHealthBar)) as PlayerHealthBar;
    }

    void Update()
    {
        if(damageTaken==false&&onTrigger==true)
        {
            TakeDamage();
            StartCoroutine("DamagePerTick");
        }
    }
    void OnTriggerEnter(Collider collider)
    {
        if(collider.gameObject.tag=="Player")
        {
            onTrigger=true;
        }
    }
    void OnTriggerExit(Collider collider)
    {
        if(collider.gameObject.tag=="Player")
        {
            onTrigger=false;
        }
    }

    IEnumerator DamagePerTick()
    {
        damageTaken=true;
        yield return new WaitForSeconds(1f);
        damageTaken=false;
    }
    void TakeDamage()
    {
        if(playerStats.Health>0)
        {
            playerStats.Health-=enemyStats.BodyDamage;
            playerHealthBar.UpdateValues();
        }
    }
    
}