using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStats : MonoBehaviour
{
    public PlayerStats playerStats;
    public string Name = "Enemy";
    public float Health = 100f;
    public float MaxHealth = 100f;
    public int Experience = 50;
    public float BasicDamage = 10f;
    public float BasicDamageMultiplier = 1f;
    public float MovementSpeed = 1f;
    public float Armor = 5f;
    public float BodyDamage = 1f;
    public bool isAlive = true;
    void Update()
    {
        if(Health<=0)
        {
            isAlive = false;
            GameObject.Find("GameController").GetComponent<GameController>().AddExperience(Experience);
            Destroy(gameObject);
        }
    }
}
