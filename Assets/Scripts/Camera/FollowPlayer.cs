using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    private Transform playerTransform;
    private Vector3 offset;
    void Start()
    {
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
        offset=transform.position-playerTransform.position;
    }
    void Update()
    {
        transform.position=playerTransform.position + offset;
    }
}
