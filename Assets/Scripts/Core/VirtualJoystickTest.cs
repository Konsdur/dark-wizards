﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VirtualJoystickTest : MonoBehaviour
{
    [SerializeField] private VirtualJoystick inputScore;
    private Rigidbody rigid;
    public CharacterController characterController;
    public PlayerStats stats;
    private void Start()
    {
        rigid = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        characterController.Move(inputScore.Direction * stats.MovementSpeed * Time.deltaTime);
    }
}
