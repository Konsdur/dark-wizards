using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public GameObject panel;
    public GameObject optionsMenu;
    public GameObject characterStatsMenu;
    public GameController gameController;

    public void Show()
    {
        panel.SetActive(true);
        gameController.image.gameObject.SetActive(false);
    }
    public void Hide()
    {
        panel.SetActive(false);
        gameController.image.gameObject.SetActive(true);
    }
    public void Resume()
    {
        Hide();
        gameController.image.gameObject.SetActive(true);
        Time.timeScale=1f;
    }
    public void OpenOptions()
    {
        optionsMenu.SetActive(true);
        gameController.TogglePauseMenu();
        gameController.image.gameObject.SetActive(false);
        Time.timeScale=0.01f;
    }
    public void CloseOptions()
    {
        optionsMenu.SetActive(false);
        gameController.TogglePauseMenu();
    }
    public void OpenCharacter()
    {   
        characterStatsMenu.SetActive(true);
        gameController.TogglePauseMenu();
        gameController.image.gameObject.SetActive(false);
        Time.timeScale=0.01f;
    }
    public void CloseCharacter()
    {
        characterStatsMenu.SetActive(false);
        gameController.TogglePauseMenu();
    }
    public void Exit()
    {
        SceneManager.LoadScene("MainMenu",LoadSceneMode.Single);
        Time.timeScale = 1f;
    }
    public void Update()
    {
         if(Input.GetKeyDown(KeyCode.Escape))
        {
            CloseOptions();
            CloseCharacter();
        }
    }
}