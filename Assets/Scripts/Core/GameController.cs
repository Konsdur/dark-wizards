using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public PlayerController player;
    public PlayerHealthBar healthBar;
    public PlayerExpBar expBar;
    public PlayerManaBar manaBar;
    public Spells spells;
    public PauseMenu pauseMenu;
    public CharacterStatsUI charStatsUI;
    public SaveOptions saveOptions;
    public Image image;

    
    void Start()
    {

        healthBar.RegisterPlayer(player);
        expBar.RegisterPlayer(player);
        manaBar.RegisterPlayer(player);
        
        charStatsUI.RegisterPlayer(player);
        InvokeRepeating("UpdateInterval",0f,0.1f);
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.T))
        {
            AddExperience(20);
        }
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            TogglePauseMenu();
        }
        if(Input.GetKeyDown(KeyCode.C))
        {
            if(pauseMenu.characterStatsMenu.activeSelf == false )
            {
                pauseMenu.OpenCharacter();
            }
            else
            {
                pauseMenu.CloseCharacter();
                Time.timeScale=1f;
            }
            pauseMenu.Hide();
        }
        if(Input.GetKeyDown(KeyCode.Alpha1))
        {
            spells.spellChoice=1;
        }
        if(Input.GetKeyDown(KeyCode.Alpha2))
        {
            spells.spellChoice=2;
        }
        if(Input.GetKeyDown(KeyCode.Alpha3))
        {
            spells.spellChoice=3;
        }
        if(Input.GetKeyDown(KeyCode.Alpha4))
        {
            spells.spellChoice=4;
        }
        if(Input.GetKeyDown(KeyCode.Alpha5))
        {
            spells.spellChoice=5;
        }
        if(Input.GetKeyDown(KeyCode.Alpha6))
        {
            spells.spellChoice=6;
        }
        if(Input.GetKeyDown(KeyCode.Alpha7))
        {
            spells.spellChoice=7;
        }
        if(Input.GetKeyDown(KeyCode.Alpha8))
        {
            spells.spellChoice=8;
        }
        if(Input.GetKeyDown(KeyCode.Alpha9))
        {
            spells.spellChoice=9;
        }
        if(player.stats.Health<=0)
        {
            player.stats.isAlive = false;
            Die();
        }
    }
    public void TogglePauseMenu()
    {
        if(pauseMenu.panel.activeSelf == true)
        {
            pauseMenu.Hide();
            Time.timeScale=1f;
        }
        else
        {
            pauseMenu.Show();
            Time.timeScale=0.01f;
        }
    }
    public void AddExperience (int value)
    {
        bool hasLeveledUp = player.stats.AddExp(value);
        if(hasLeveledUp)
        {
            //Do something
        }
        
    }
    public void UpdateInterval()
    {
        expBar.UpdateValues();
        manaBar.UpdateValues();
    }
    private void Die()
   {
        SceneManager.LoadScene("Survival",LoadSceneMode.Single);
   }
}
