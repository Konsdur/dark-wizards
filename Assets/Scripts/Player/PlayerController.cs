using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerController : MonoBehaviour
{
    public PlayerStats stats;
    public CharacterController controller;
    void Update()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical =Input.GetAxisRaw("Vertical");
        Vector3 direction = new Vector3(horizontal, 0f, vertical).normalized;    
        if(direction.magnitude>=0.1f)
        {
            controller.Move(direction*stats.MovementSpeed*Time.deltaTime);
        }
        
    }
}
