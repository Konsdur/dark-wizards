using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorSpell : MonoBehaviour
{

    private EnemyStats enemyStats;
    public PlayerStats playerStats;
    public DetectCursorPosition cursorPosition;
    public int spellChoice = 0;
    public float spellCost = 10.0f;
    public static float spellCooldown = 5.0f;
    public bool isReadyToUse = true;
    public GameObject meteor;
    public float spellDamage;
    public GameObject gameObject;
    IEnumerator SpellCooldown()
    {
        yield return new WaitForSeconds(spellCooldown);
        isReadyToUse = true;
    }
   public void Meteor()
    {
        if(playerStats.Mana>=spellCost&&isReadyToUse == true)
        {
            isReadyToUse = false;
            playerStats.Mana -= spellCost;
            //cursorPosition.CursorWorldPosition(); --- POTRZEBA ZMIENIENIA NA INFRONT OF CHARACTER PRZY ZMIANIE KIERUNKU OBRACANIA
            Instantiate(meteor, new Vector3(gameObject.transform.position.x, 15, gameObject.transform.position.z + 5), Quaternion.identity);
            StartCoroutine("SpellCooldown");
        }
        else
        {
            print ("You dont have mana or spell is not ready yet!");
        }
    }
    public void DoDamageToEnemy(List<GameObject> enemies)
    {
        foreach(GameObject enemy in enemies)
        {
            enemyStats = enemy.GetComponent<EnemyStats>();
            if(enemyStats.Health>0)
            {
                spellDamage = (enemyStats.BasicDamage * enemyStats.BasicDamageMultiplier) + 10f;
                enemyStats.Health-=spellDamage;
            }
        }
    }
}