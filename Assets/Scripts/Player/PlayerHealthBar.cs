using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealthBar : MonoBehaviour
{
    PlayerController player;
    public Image fillImage;
    public Text text;
    public void RegisterPlayer(PlayerController p)
    {
        player = p;
    }
    public void UpdateValues()
    {
        fillImage.fillAmount = player.stats.Health / player.stats.MaxHealth;
        text.text = player.stats.Health + "/" + player.stats.MaxHealth;
    }
}
