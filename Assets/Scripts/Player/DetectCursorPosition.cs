using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectCursorPosition : MonoBehaviour
{
    public Vector3 worldPosition;
    public void CursorWorldPosition()
    {
        Plane plane = new Plane(Vector3.up, 0);
        float distance;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (plane.Raycast(ray, out distance))
        {
            worldPosition = ray.GetPoint(distance);
        }
    }
}
