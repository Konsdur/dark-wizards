using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour
{
    private static PlayerHealthBar playerHealthBar;
    public string Name = "Player";
    public int Level = 1;
    public int Experience = 0;
    public float Health = 100f;
    public float MaxHealth = 100f;
    public float Mana = 100f;
    public float MaxMana = 100f;
    public float BasicDamage = 10f;
    public float BasicDamageMultiplier = 1.1f;
    public float HealthRegen = 1f;
    public float ManaRegen = 1f;
    public float MovementSpeed = 5f;
    public float Armor = 5f;
    public bool isAlive = true;

    private void Start()
    {
        playerHealthBar = GameObject.FindObjectOfType(typeof(PlayerHealthBar)) as PlayerHealthBar;
    }
    public bool AddExp(int value)
   {
       Experience+=value;
       if(Experience>=Level*100)
       {
           Experience-= Level*100;
           Level++;
           MaxHealth+=MaxHealth;
           Health = MaxHealth;
           playerHealthBar.UpdateValues();
           return true;
       }
       return false;
   }
}
