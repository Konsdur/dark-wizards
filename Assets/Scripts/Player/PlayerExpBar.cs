using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerExpBar : MonoBehaviour
{
    PlayerController player;
    public Image fillImage;
    public void RegisterPlayer(PlayerController p)
    {
        player = p;
    }
    public void UpdateValues()
    {
        fillImage.fillAmount = (float)player.stats.Experience / ((float)player.stats.Level*100.0f);
    }
}
