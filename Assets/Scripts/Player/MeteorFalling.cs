using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorFalling : MonoBehaviour
{    
    public Transform meteor;
    public Vector3 Shift = new Vector3(0, 0.005f, 0);

    public void Start()
    {
        meteor = GetComponent<Transform>();
    }
    void Update()
    {
        if(meteor.position.y > 0.5)
        {
            InvokeRepeating("startFalling",0f,0.1f);
        }
        else
        {
            Destroy(gameObject);        
        }
    }
    public void startFalling()
    {
        meteor.position -= Shift;
    }
}