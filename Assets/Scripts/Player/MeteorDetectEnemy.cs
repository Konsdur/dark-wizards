using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorDetectEnemy : MonoBehaviour
{
    public bool damageTaken = false;
    public bool onTrigger = false;

    public List<GameObject> enemies = new List<GameObject>();
    void Update()
    {
        if(damageTaken==false&&onTrigger==true)
        {
            GameObject.Find("Spells").GetComponent<MeteorSpell>().DoDamageToEnemy(enemies);
            damageTaken=true;
        }
    }
    void OnTriggerEnter(Collider collider)
    {
        if(collider.gameObject.tag=="Enemy")
        {
            onTrigger=true;
            if(!enemies.Contains(collider.gameObject))
            {
                enemies.Add(collider.gameObject);
            };
            Debug.Log("Wróg wchodzi");
        }
    }
    void OnTriggerExit(Collider collider)
    {
        if(collider.gameObject.tag=="Enemy")
        {
            if(enemies.Contains(collider.gameObject))
            {
                enemies.Remove(collider.gameObject);
            };
            onTrigger=false;
            damageTaken=false;
            Debug.Log("Wróg wychodzi");
        }
    }
}