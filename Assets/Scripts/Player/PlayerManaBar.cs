using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerManaBar : MonoBehaviour
{
    PlayerController player;
    public Image fillImage;
    public Text text;
    public void RegisterPlayer(PlayerController p)
    {
        player = p;
    }
    public void UpdateValues()
    {
        fillImage.fillAmount = player.stats.Mana / player.stats.MaxMana;
        text.text = player.stats.Mana + "/" + player.stats.MaxMana;
    }
}
