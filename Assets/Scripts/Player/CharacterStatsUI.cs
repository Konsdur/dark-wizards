using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterStatsUI : MonoBehaviour
{
    public Text Name;
    public Text Level;
    public Text Experience;
    public Text BasicDamage;
    public Text DamageMultiplier;
    public Text HealthRegen;
    public Text ManaRegen;
    public Text MovementSpeed;
    public Text Armor;
    public PlayerController player;
    public void RegisterPlayer(PlayerController p)
    {
        player=p;
    }

    void Update()
    {
        if(isActiveAndEnabled)
        {
            UpdateUI();
        }
    }
    public void UpdateUI()
    {
        Name.text = player.stats.Name;
        Level.text = player.stats.Level.ToString();
        Experience.text = player.stats.Experience.ToString();
        BasicDamage.text = player.stats.BasicDamage.ToString();
        DamageMultiplier.text = player.stats.BasicDamageMultiplier.ToString();
        HealthRegen.text = player.stats.HealthRegen.ToString();
        ManaRegen.text = player.stats.ManaRegen.ToString();
        MovementSpeed.text = player.stats.MovementSpeed.ToString();
        Armor.text = player.stats.Armor.ToString();
    }
}