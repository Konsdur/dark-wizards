using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class OptionsChanger : MonoBehaviour
{
    public Slider masterSlider;
    public Slider musicSlider;
    public Slider effectsSlider;
    public Slider voiceSlider;
    //public Dropdown frames;

    public void Start()
    {
        masterSlider.value = PlayerPrefs.GetFloat("MasterVol");
        musicSlider.value = PlayerPrefs.GetFloat("MusicVol");
        effectsSlider.value = PlayerPrefs.GetFloat("EffectsVol");
        voiceSlider.value = PlayerPrefs.GetFloat("VoiceVol");
        //frames.value = PlayerPrefs.GetInt("fpsint");
    }
}
