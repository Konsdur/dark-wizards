using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Audio;

public class ControlMenu : MonoBehaviour
{

    public void Start()
    {
        //Screen.SetResolution(PlayerPrefs.GetInt("ResolutionX"), PlayerPrefs.GetInt("ResolutionY"), true);
    }


    public void GameStart()
    {
        SceneManager.LoadScene("Gamemodes", LoadSceneMode.Single);
    }

    public void Survival()
    {
        SceneManager.LoadScene("Survival", LoadSceneMode.Single);
    }

    public void Campaign()
    {
        SceneManager.LoadScene("Campaign", LoadSceneMode.Single);
    }

    public void Cooperative()
    {
        SceneManager.LoadScene("Cooperative", LoadSceneMode.Single);
    }

    public void Options()
    {
        SceneManager.LoadScene("Options", LoadSceneMode.Single);
    }

    public void Help()
    {
        SceneManager.LoadScene("Help", LoadSceneMode.Single);
    }

    public void Credits()
    {
        SceneManager.LoadScene("Credits", LoadSceneMode.Single);
    }

    public void GameExit()
    {
        Application.Quit();
    }

    public void GameBackMenu()
    {
        SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
    }
 
    public void Save()
    {
        PlayerPrefs.Save();
    }    
}
