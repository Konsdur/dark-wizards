using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class VolumeChanger : MonoBehaviour
{
    public AudioMixer mixer;

    public void MasterSetLevel(float sliderValue)
    {
        mixer.SetFloat("MasterVol", Mathf.Log10(sliderValue) * 20);
        PlayerPrefs.SetFloat("MasterVol", sliderValue);
        PlayerPrefs.SetFloat("MasterVolMenu", Mathf.Log10(sliderValue) * 20);
    }
    public void MusicSetLevel(float sliderValue)
    {
        mixer.SetFloat("MusicVol", Mathf.Log10(sliderValue) * 20);
        PlayerPrefs.SetFloat("MusicVol", sliderValue);
        PlayerPrefs.SetFloat("MusicVolMenu", Mathf.Log10(sliderValue) * 20);
    }
    public void EffectsSetLevel(float sliderValue)
    {
        mixer.SetFloat("EffectsVol", Mathf.Log10(sliderValue) * 20);
        PlayerPrefs.SetFloat("EffectsVol", sliderValue);
        PlayerPrefs.SetFloat("EffectsVolMenu", Mathf.Log10(sliderValue) * 20);
    }
    public void VoiceSetLevel(float sliderValue)
    {
        mixer.SetFloat("VoiceVol", Mathf.Log10(sliderValue) * 20);
        PlayerPrefs.SetFloat("VoiceVol", sliderValue);
        PlayerPrefs.SetFloat("VoiceVolMenu", Mathf.Log10(sliderValue) * 20);
    }
}