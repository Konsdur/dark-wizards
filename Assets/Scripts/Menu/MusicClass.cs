using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

public class MusicClass : MonoBehaviour
{
    public AudioMixer mixer;

    public void Start()
    {
        mixer.SetFloat("MasterVol", PlayerPrefs.GetFloat("MasterVolMenu"));
        mixer.SetFloat("MusicVol", PlayerPrefs.GetFloat("MusicVolMenu"));
        mixer.SetFloat("EffectsVol", PlayerPrefs.GetFloat("EffectsVolMenu"));
        mixer.SetFloat("VoiceVol", PlayerPrefs.GetFloat("VoiceVolMenu"));
    }

    private void Awake()
    {
        //Scene scene = SceneManager.GetActiveScene();

        GameObject[] objs = GameObject.FindGameObjectsWithTag("Music");
        //if (scene.name == "Survival")
        //{

            //}
        if (objs.Length > 1)
        {
            Destroy(this.gameObject);
        }
        else
        {
            DontDestroyOnLoad(this.gameObject);
        }
    }

}

