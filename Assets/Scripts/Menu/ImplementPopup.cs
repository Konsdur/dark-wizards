using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ImplementPopup : MonoBehaviour
{
    public GameObject popup;
    public GameObject ui;
    private bool active;

    public void Start()
    {
        GameObject.FindGameObjectWithTag("Implement").SetActive(!popup.activeSelf);
    }

    public void Unavailable()
    {
        popup.SetActive(!popup.activeSelf);
        ui.SetActive(!popup.activeSelf);
        active = true;
    }

    public void Update()
    {

        if(!active)
        {
            if(Input.GetKeyUp(KeyCode.Escape))
            {
                SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
            }    
        }
    }
    public void LateUpdate()
    {
        if (active)
        {
            if (Input.GetKeyUp(KeyCode.Escape) || Input.GetMouseButtonUp(1))
            {
                active = false;
                popup.SetActive(!popup.activeSelf);
                ui.SetActive(!popup.activeSelf);
            }
        }
    }

}
